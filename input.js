// node input.js x y density
if(process.argv.length < 3) {
    console.log("program x y density\n");
    return
}
x = process.argv[2];
y = process.argv[3];
density = process.argv[4];
i = 0;
j = 0;
console.log(y);
while (i < y)
{
    j = 0;
    while (j < x)
    {
        if ((Math.random()*(y-0)+0) * 2 < density)
            process.stdout.write("o");
        else
            process.stdout.write(".");
        j++;
    }
    process.stdout.write("\n");
    i++;
}